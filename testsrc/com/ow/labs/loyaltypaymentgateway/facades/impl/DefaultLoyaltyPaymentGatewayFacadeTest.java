package com.ow.labs.loyaltypaymentgateway.facades.impl;

import com.ow.labs.loyaltypaymentgateway.constants.LoyaltypaymentgatewayConstants.PaymentGateways;
import com.ow.labs.loyaltypaymentgateway.facades.LoyaltyPaymentGatewayFacade;
import com.ow.labs.loyaltypaymentgateway.wsservice.LoyaltyPaymentGatewayService;
import com.ow.labs.loyaltypaymentgateway.wsservice.exceptions.LoyaltyTransactionException;
import com.ow.labs.loyaltypaymentgateway.wsservice.factory.LoyaltyPaymentGatewayFactory;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;


/**
 * @author carlos-ow.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultLoyaltyPaymentGatewayFacadeTest
{
	private static final String INVALID_CUSTOMER = "invalidmembership";

	@InjectMocks
	LoyaltyPaymentGatewayFacade facade =
			new DefaultLoyaltyPaymentGatewayFacade();

	@Mock
	UserService userService;

	@Mock
	CustomerModel validCustomer;

	@Mock
	LoyaltyPaymentGatewayFactory factory;

	@Mock
	LoyaltyPaymentGatewayService anyValidService;

	@Before
	public void setUp() throws LoyaltyTransactionException
	{
		when(userService.getCurrentUser())
				.thenReturn(validCustomer);

		// indicate the factory what's the "second level" implementation to use (generic one)
		when(factory.getInstanceForGateway(any(PaymentGateways.class)))
				.thenReturn(anyValidService);

		// for any request we have, just return true
		when(anyValidService.pay(any(String.class), any(BigDecimal.class)))
				.thenReturn(Boolean.TRUE);

		// specifically for the invalid customer's info, return that the transaction could not be processed properly
		when(anyValidService.pay(eq(INVALID_CUSTOMER), any(BigDecimal.class)))
				.thenReturn(Boolean.FALSE);
	}

	/**
	 * Transaction should be successful.
	 *
	 * @throws Exception
	 */
	@Test
	public void payWithGarfield() throws Exception
	{
		final boolean success =
				facade.pay(PaymentGateways.GARFIELD.getCode(), BigDecimal.valueOf(10.0));

		assertThat(success)
				.isTrue();
	}

	@Test
	public void failedTransactionForCustomer() throws Exception
	{
		when(validCustomer.getUid())
				.thenReturn(INVALID_CUSTOMER);

		final boolean success =
				facade.pay(PaymentGateways.GARFIELD.getCode(), BigDecimal.valueOf(10.0));

		assertThat(success)
				.isFalse();
	}

	@Test(expected = LoyaltyTransactionException.class)
	public void payWithGarfieldFailedOnCommunication() throws Exception
	{
		when(anyValidService.pay(any(String.class), any(BigDecimal.class)))
				.thenThrow(LoyaltyTransactionException.class);

		facade.pay(PaymentGateways.GARFIELD.getCode(), BigDecimal.valueOf(10.0));
	}

	/**
	 * Should throw an exception since the payment gateway is not valid / supported.
	 *
	 * @throws Exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void paymentMethodDoesNotExist() throws Exception
	{
		facade.pay("fake", BigDecimal.valueOf(10.0));
	}

	/**
	 * Should throw an exception since the amount is lower than a valid element.
	 *
	 * @throws Exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void amountIsNotValid() throws Exception
	{
		facade.pay(PaymentGateways.GARFIELD.getCode(), BigDecimal.valueOf(-1));
	}
}
