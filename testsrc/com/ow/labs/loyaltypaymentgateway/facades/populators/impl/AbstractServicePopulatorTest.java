package com.ow.labs.loyaltypaymentgateway.facades.populators.impl;

import com.ow.labs.loyaltypaymentgateway.facades.dtos.WSLoyaltyRequest;
import com.ow.labs.loyaltypaymentgateway.facades.populators.WSLoyaltyPaymentGatewayPopulator;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import reactor.util.StringUtils;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;


/**
 * @author carlos-ow.
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class AbstractServicePopulatorTest
{
	final WSLoyaltyPaymentGatewayPopulator populator =
			new AbstractServicePopulator();

	/**
	 * Should serialize an object which should not be an empty element.
	 */
	@Test
	public void testObjectSerialization()
	{
		final WSLoyaltyRequest dto = new WSLoyaltyRequest();
		dto.setMembership("some-membership-id-for-the-customer");
		dto.setAmount("10.0");

		final String result = populator.createRequest(dto);

		assertThat(StringUtils.isEmpty(result))
				.isFalse();

		// TODO: change the ugly system#out for a better logging system
		System.out.println("result \n" + result);
	}
}
