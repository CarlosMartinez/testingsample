package com.ow.labs.loyaltypaymentgateway.wsservice.impl;

import com.ow.labs.loyaltypaymentgateway.facades.populators.WSLoyaltyPaymentGatewayPopulator;
import com.ow.labs.loyaltypaymentgateway.facades.populators.impl.AbstractServicePopulator;
import com.ow.labs.loyaltypaymentgateway.wsservice.LoyaltyPaymentGatewayService;
import com.ow.labs.loyaltypaymentgateway.wsservice.exceptions.LoyaltyTransactionException;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;


/**
 * @author carlos-ow.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultGarfieldPaymentGatewayServiceUNITTest
{
	// no need to inject mocks since we have specific
	// implementations that can be injected manually
	private LoyaltyPaymentGatewayService service =
			new DefaultGarfieldPaymentGatewayService();

	private WSLoyaltyPaymentGatewayPopulator basePopulator =
			new AbstractServicePopulator();

	private DefaultGarfieldPaymentGatewayService getAsImpl()
	{
		return (DefaultGarfieldPaymentGatewayService) service;
	}

	@Before
	public void setUp()
	{
		getAsImpl().setEndpoint("http://localhost:3030/loyalty/gateways/garfield");
		getAsImpl().setPopulator(basePopulator);
	}

	/**
	 * Should place the request to the backend successfully.
	 */
	@Test
	public void testSendRequestToEndpoint() throws Exception
	{
		final boolean transactionStatus =
				service.pay(
						"some-membership", BigDecimal.valueOf(1000));

		assertThat(transactionStatus)
				.isTrue();
	}

	/**
	 * Transaction should not be valid.
	 *
	 * @throws Exception
	 */
	@Test
	public void testRequestIsNotAcceptedByBackend() throws Exception
	{
		final boolean transaction = service.pay("failing-membership", BigDecimal.TEN);

		assertThat(transaction)
				.isFalse();
	}

}
