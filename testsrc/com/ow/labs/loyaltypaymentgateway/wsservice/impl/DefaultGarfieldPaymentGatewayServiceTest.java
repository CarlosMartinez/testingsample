package com.ow.labs.loyaltypaymentgateway.wsservice.impl;

import com.ow.labs.loyaltypaymentgateway.wsservice.LoyaltyPaymentGatewayService;
import com.ow.labs.loyaltypaymentgateway.wsservice.exceptions.LoyaltyTransactionException;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import org.junit.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;


/**
 * @author carlos-ow.
 */
@IntegrationTest
public class DefaultGarfieldPaymentGatewayServiceTest extends ServicelayerTransactionalTest
{
	@Resource(name = "garfieldPaymentGatewayService")
	private LoyaltyPaymentGatewayService garfieldPaymentGatewayService;


	/**
	 * Should place the request to the backend successfully.
	 */
	@Test
	public void testSendRequestToEndpoint() throws Exception
	{
		final boolean transactionStatus =
				garfieldPaymentGatewayService.pay(
						"some-membership", BigDecimal.valueOf(1000));

		assertThat(transactionStatus)
				.isTrue();
	}

	/**
	 * Transaction should not be successful.
	 *
	 * @throws Exception
	 */
	@Test
	public void testRequestIsNotAcceptedByBackend() throws Exception
	{
		final boolean status =
				garfieldPaymentGatewayService.pay("failing-membership", BigDecimal.TEN);

		assertThat(status)
				.isFalse();
	}

}
