package com.ow.labs.loyaltypaymentgateway.controller;

import org.slf4j.Logger;
import org.springframework.web.client.RestTemplate;


/**
 * @author carlos-ow.
 */
public abstract class AbstractEndpointTest
{
	abstract String getBaseUrl();

	protected <RequestType> void post(final String resource, RequestType payload)
	{
		RestTemplate request = new RestTemplate();
		request.postForLocation(getUrl(resource), payload);
	}

	protected <ReturnType, RequestType> ReturnType
	postWithReturn(final String resource, final RequestType payload, final Class<ReturnType> returnType)
	{
		RestTemplate request = new RestTemplate();
		return request.postForObject(getUrl(resource), payload, returnType);
	}

	protected String getUrl(final String resource)
	{
		return getBaseUrl() + resource;
	}
}
