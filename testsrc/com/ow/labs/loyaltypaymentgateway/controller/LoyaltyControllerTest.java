package com.ow.labs.loyaltypaymentgateway.controller;

import com.ow.labs.loyaltypaymentgateway.data.GenericResponseDto;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;


/**
 * @author carlos-ow.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LoyaltyControllerTest extends AbstractEndpointTest
{

	protected String getBaseUrl()
	{
		return "http://localhost:9001/loyaltypaymentgateway/loyalty/payment/gateways";
	}


	@Test
	public void testGarfieldPayment()
	{
		final GenericResponseDto response =
				postWithReturn("/garfield", BigDecimal.ONE, GenericResponseDto.class);

		assertThat(response.getError())
				.isEmpty();

		assertThat(response.getSuccess())
				.isEqualTo(Boolean.TRUE);
	}
}
