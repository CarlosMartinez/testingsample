
module.exports = app => {

  // start the app!
  app.listen(app.get('port'), () => {
    console.log(`Server running on http://localhost:${app.get('port')}`)
  })
}
