import bodyParser from 'body-parser'

module.exports = app => {
  // format the JSON responses - "prettify"
  app.set('json spaces', 2)
  app.set('port', process.env.PORT || 3030)

  // setup JSON support
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: false }))
}
