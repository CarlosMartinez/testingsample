# About

This project is meant to be used for the TDD examples of the brownbag so that we can
demostrate how to design and write Integration Tests

# Setup

## Requirements

* NodeJS version 8.X
* npm 5.X

## Run the example

First, you have to install the dependencies:

`npm install`

Finally, run the code `npm start`

That's it :)


### Author

Carlos José Martínez Arenas - ObjectWave
