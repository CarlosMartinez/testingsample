import express from 'express'
import consign from 'consign'

const app = express()

consign()
  .include('app/config.js')
  .then('routes')
  .include('app/bootloader.js')
  .into(app)
