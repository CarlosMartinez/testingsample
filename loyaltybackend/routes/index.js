import uuid from 'uuid/v4'

const FAILED_MEMBERSHIP = 'failing-membership'

/**
 * Checks if the request contains the failing memberhsip.
 *
 * @param req Request to be checked.
 * @return True if it contains the failing membership, false otherwise.
 */
const isFailing = req => {
  return req.body.membership === FAILED_MEMBERSHIP
}

module.exports = app => {
  app.get('/', (req, res) => {
    res.json({
      urls: [
        {
          method: 'POST',
          url: '/loyalty/gateways/garfield'
        },
        {
          method: 'POST',
          url: '/loyalty/gateways/roco'
        },
        {
          method: 'POST',
          url: '/loyalty/gateways/invaderzim'
        }
      ],
      payload: {
        membership: 'The user\'s membership',
        amount: 'The amount to be charged'
      }
    })
  })

  app.post('/loyalty/gateways/:gateway', (req, res, next) => {
    console.log(`getting request on gateway ${req.params.gateway}`)
    // had some issues so had to print the headers for a while, but pay no attention to it :V
    // console.log(`headers ${JSON.stringify(req.headers)}`)
    console.log(`params: ${JSON.stringify(req.body)}`)
    next()
  })


  app.post('/loyalty/gateways/garfield', (req, res) => {
    const status = isFailing(req) ? 'failed' : 'success'

    res.json({
      transactionId: uuid(),
      status
    })
  })

  app.post('/loyalty/gateways/roco', (req, res) => {
    const successful = !isFailing(req)

    res.json({
      transactionId: uuid(),
      successful,
      remaining: 10
    })
  })

  app.post('/loyalty/gateways/invaderzim', (req, res) => {
    const successful = !isFailing(req)

    res.json({
      transaction: uuid(),
      successful,
      remainingAmount: 15.04
    })
  })
}
