# Project

This extension is only an example of multiple best practices, hacks, and recommendations
to develop testable components from scratch

## Setup

In order to run the code used for the backend, you need to check the project `loyaltybackend`, there's a *README.md* file that will help you set up and run it on your local environment
