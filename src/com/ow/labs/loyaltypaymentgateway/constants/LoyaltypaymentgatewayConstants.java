/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.ow.labs.loyaltypaymentgateway.constants;

/**
 * Global class for all Loyaltypaymentgateway constants. You can add global constants for your extension into this class.
 */
public final class LoyaltypaymentgatewayConstants extends GeneratedLoyaltypaymentgatewayConstants
{
	public static final String EXTENSIONNAME = "loyaltypaymentgateway";

	private LoyaltypaymentgatewayConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "loyaltypaymentgatewayPlatformLogo";

	public enum PaymentGateways
	{
		GARFIELD("GARFIELD"),
		ROCO("ROCO"),
		ZIM("ZIM");

		private String code;

		public String getCode()
		{
			return this.code;
		}

		PaymentGateways(final String code)
		{
			this.code = code;
		}

		@Override
		public String toString()
		{
			return "Code: " + this.code;
		}
	}
}
