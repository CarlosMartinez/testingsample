package com.ow.labs.loyaltypaymentgateway.facades;

import com.ow.labs.loyaltypaymentgateway.constants.LoyaltypaymentgatewayConstants.PaymentGateways;
import com.ow.labs.loyaltypaymentgateway.wsservice.exceptions.LoyaltyTransactionException;

import java.math.BigDecimal;


/**
 * @author carlos-ow.
 */
public interface LoyaltyPaymentGatewayFacade
{
	/**
	 * Calls the given payment gateway to send the request to the corresponding backend.
	 *
	 * @param gateway Payment gateway to be used.
	 * @param amount  Amount to be paid.
	 * @return If transaction was correct or not.
	 * @throws LoyaltyTransactionException
	 */
	boolean pay(final String gateway, final BigDecimal amount) throws LoyaltyTransactionException;
}
