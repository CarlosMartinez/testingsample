package com.ow.labs.loyaltypaymentgateway.facades.dtos;

/**
 * @author carlos-ow.
 */
public class WSLoyaltyGarfieldResponse
{
	private String transactionId;
	private String status;

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(String transactionId)
	{
		this.transactionId = transactionId;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}
}
