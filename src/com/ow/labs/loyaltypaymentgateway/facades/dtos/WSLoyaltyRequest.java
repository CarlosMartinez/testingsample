package com.ow.labs.loyaltypaymentgateway.facades.dtos;

/**
 * @author carlos-ow.
 */
public class WSLoyaltyRequest
{
	private String membership;
	private String amount;

	public String getMembership()
	{
		return membership;
	}

	public void setMembership(String membership)
	{
		this.membership = membership;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(String amount)
	{
		this.amount = amount;
	}
}
