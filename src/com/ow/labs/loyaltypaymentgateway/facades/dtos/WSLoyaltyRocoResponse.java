package com.ow.labs.loyaltypaymentgateway.facades.dtos;

/**
 * @author carlos-ow.
 */
public class WSLoyaltyRocoResponse
{
	private String transactionId;
	private boolean successful;
	private Integer remaining;

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(String transactionId)
	{
		this.transactionId = transactionId;
	}

	public boolean isSuccessful()
	{
		return successful;
	}

	public void setSuccessful(boolean successful)
	{
		this.successful = successful;
	}

	public Integer getRemaining()
	{
		return remaining;
	}

	public void setRemaining(Integer remaining)
	{
		this.remaining = remaining;
	}
}
