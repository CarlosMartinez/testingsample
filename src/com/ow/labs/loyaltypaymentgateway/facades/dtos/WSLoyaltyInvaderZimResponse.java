package com.ow.labs.loyaltypaymentgateway.facades.dtos;

/**
 * @author carlos-ow.
 */
public class WSLoyaltyInvaderZimResponse
{
	private String transaction;
	private boolean successful;
	private Double remainingAmount;

	public String getTransaction()
	{
		return transaction;
	}

	public void setTransaction(String transaction)
	{
		this.transaction = transaction;
	}

	public boolean isSuccessful()
	{
		return successful;
	}

	public void setSuccessful(boolean successful)
	{
		this.successful = successful;
	}

	public Double getRemainingAmount()
	{
		return remainingAmount;
	}

	public void setRemainingAmount(Double remainingAmount)
	{
		this.remainingAmount = remainingAmount;
	}
}
