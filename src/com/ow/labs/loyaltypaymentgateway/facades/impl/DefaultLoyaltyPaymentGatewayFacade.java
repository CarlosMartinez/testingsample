package com.ow.labs.loyaltypaymentgateway.facades.impl;

import com.ow.labs.loyaltypaymentgateway.constants.LoyaltypaymentgatewayConstants.PaymentGateways;
import com.ow.labs.loyaltypaymentgateway.facades.LoyaltyPaymentGatewayFacade;
import com.ow.labs.loyaltypaymentgateway.wsservice.LoyaltyPaymentGatewayService;
import com.ow.labs.loyaltypaymentgateway.wsservice.exceptions.LoyaltyTransactionException;
import com.ow.labs.loyaltypaymentgateway.wsservice.factory.LoyaltyPaymentGatewayFactory;
import com.ow.labs.loyaltypaymentgateway.wsservice.factory.impl.DefaultLoyaltyPaymentGatewayFactory;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;


/**
 * @author carlos-ow.
 */
public class DefaultLoyaltyPaymentGatewayFacade implements LoyaltyPaymentGatewayFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultLoyaltyPaymentGatewayFacade.class);

	private UserService userService;
	private LoyaltyPaymentGatewayFactory paymentGatewayFactory;

	/**
	 * @see LoyaltyPaymentGatewayFacade#pay(String, BigDecimal).
	 */
	@Override
	public boolean pay(final String gatewayStr, final BigDecimal amount)
			throws LoyaltyTransactionException
	{
		if (amount.compareTo(BigDecimal.ZERO) <= 0)
		{
			throw new IllegalArgumentException("Amount should be positive");
		}

		final PaymentGateways gateway = PaymentGateways.valueOf(gatewayStr);

		LOG.trace("Before getting the service implementation");
		// find the corresponding service implementation
		final LoyaltyPaymentGatewayService service =
				getPaymentGatewayFactory().getInstanceForGateway(gateway);

		LOG.debug("Using {} class for gateway {}",
				service.getClass().getSimpleName(), gateway);

		// execute the transaction for it
		return service.pay(getUsersCard(), amount);
	}


	/**
	 * Gets the current user's card id.
	 *
	 * @return The user's card id.
	 */
	protected String getUsersCard()
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		// XXX: here, you would retrieve the customer's specific information
		return currentUser.getUid();
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(UserService userService)
	{
		this.userService = userService;
	}

	public LoyaltyPaymentGatewayFactory getPaymentGatewayFactory()
	{
		return paymentGatewayFactory;
	}

	@Required
	public void setPaymentGatewayFactory(
			LoyaltyPaymentGatewayFactory paymentGatewayFactory)
	{
		this.paymentGatewayFactory = paymentGatewayFactory;
	}
}
