package com.ow.labs.loyaltypaymentgateway.facades.populators.impl;

import com.ow.labs.loyaltypaymentgateway.facades.dtos.WSLoyaltyRequest;
import com.ow.labs.loyaltypaymentgateway.facades.populators.WSLoyaltyPaymentGatewayPopulator;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;


/**
 * @author carlos-ow.
 */
public class AbstractServicePopulator implements WSLoyaltyPaymentGatewayPopulator
{
	private static final Logger LOG = LoggerFactory.getLogger(AbstractServicePopulator.class);

	/**
	 * @see WSLoyaltyPaymentGatewayPopulator#createRequest(WSLoyaltyRequest).
	 */
	@Override
	public String createRequest(final WSLoyaltyRequest dto)
	{
		final ObjectMapper serializer = new ObjectMapper();
		String json = null;

		final DecimalFormat formatter = new DecimalFormat();
		formatter.setMaximumFractionDigits(2);

		try
		{
			// serialize it
			json = serializer.writeValueAsString(dto);
		}
		catch (final Exception ex)
		{
			LOG.error("Error while trying to serialize the object", ex);
		}

		return json;
	}
}
