package com.ow.labs.loyaltypaymentgateway.facades.populators;

import com.ow.labs.loyaltypaymentgateway.facades.dtos.WSLoyaltyRequest;


/**
 * @author carlos-ow.
 */
public interface WSLoyaltyPaymentGatewayPopulator
{
	/**
	 * Creates the request to be sent to the service.
	 *
	 * @param dto The request dto to be serialized as a JSON format string.
	 * @return The JSON string to be included as the body of the HTTP request.
	 */
	String createRequest(final WSLoyaltyRequest dto);
}
