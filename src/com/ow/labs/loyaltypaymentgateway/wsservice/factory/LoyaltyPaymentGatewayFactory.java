package com.ow.labs.loyaltypaymentgateway.wsservice.factory;

import com.ow.labs.loyaltypaymentgateway.constants.LoyaltypaymentgatewayConstants;
import com.ow.labs.loyaltypaymentgateway.wsservice.LoyaltyPaymentGatewayService;


/**
 * @author carlos-ow.
 */
public interface LoyaltyPaymentGatewayFactory
{
	/**
	 * Gets the corresponding instance for the required gateway.
	 *
	 * @param gateway Gateway to retrieve the instance of.
	 * @return The specific service implementation.
	 */
	LoyaltyPaymentGatewayService getInstanceForGateway(final LoyaltypaymentgatewayConstants.PaymentGateways gateway);
}
