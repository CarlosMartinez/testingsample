package com.ow.labs.loyaltypaymentgateway.wsservice.factory.impl;

import com.ow.labs.loyaltypaymentgateway.constants.LoyaltypaymentgatewayConstants.PaymentGateways;
import com.ow.labs.loyaltypaymentgateway.wsservice.LoyaltyPaymentGatewayService;
import com.ow.labs.loyaltypaymentgateway.wsservice.factory.LoyaltyPaymentGatewayFactory;


/**
 * Internally handles the creation / retrieval of the corresponding services.
 *
 * @author carlos-ow.
 */
public class DefaultLoyaltyPaymentGatewayFactory implements LoyaltyPaymentGatewayFactory
{
	private LoyaltyPaymentGatewayService garfieldService;
	private LoyaltyPaymentGatewayService invaderZimService;
	private LoyaltyPaymentGatewayService rocoService;


	/**
	 * @see LoyaltyPaymentGatewayFactory#getInstanceForGateway(PaymentGateways).
	 */
	@Override
	public LoyaltyPaymentGatewayService getInstanceForGateway(final PaymentGateways gateway)
	{
		final LoyaltyPaymentGatewayService service;

		// get the corresponding service for the payment gateway
		if (PaymentGateways.GARFIELD.equals(gateway))
		{
			service = getGarfieldService();
		}
		else if (PaymentGateways.ZIM.equals(gateway))
		{
			service = getInvaderZimService();
		}
		else if (PaymentGateways.ROCO.equals(gateway))
		{
			service = getRocoService();
		}
		else
		{
			throw new IllegalArgumentException("Invalid / unsupported payment gateway " + gateway);
		}

		return service;
	}

	public LoyaltyPaymentGatewayService getGarfieldService()
	{
		return garfieldService;
	}

	public void setGarfieldService(LoyaltyPaymentGatewayService garfieldService)
	{
		this.garfieldService = garfieldService;
	}

	public LoyaltyPaymentGatewayService getInvaderZimService()
	{
		return invaderZimService;
	}

	public void setInvaderZimService(LoyaltyPaymentGatewayService invaderZimService)
	{
		this.invaderZimService = invaderZimService;
	}

	public LoyaltyPaymentGatewayService getRocoService()
	{
		return rocoService;
	}

	public void setRocoService(LoyaltyPaymentGatewayService rocoService)
	{
		this.rocoService = rocoService;
	}
}
