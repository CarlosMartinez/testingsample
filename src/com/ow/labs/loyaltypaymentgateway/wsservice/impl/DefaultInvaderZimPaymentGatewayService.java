package com.ow.labs.loyaltypaymentgateway.wsservice.impl;

import com.ow.labs.loyaltypaymentgateway.facades.dtos.WSLoyaltyInvaderZimResponse;
import com.ow.labs.loyaltypaymentgateway.facades.dtos.WSLoyaltyRequest;
import com.ow.labs.loyaltypaymentgateway.wsservice.LoyaltyPaymentGatewayService;
import com.ow.labs.loyaltypaymentgateway.wsservice.exceptions.LoyaltyTransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;


/**
 * @author carlos-ow.
 */
public class DefaultInvaderZimPaymentGatewayService extends AbstractLoyaltyPaymentGatewayService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultInvaderZimPaymentGatewayService.class);

	protected Logger getLog()
	{
		return LOG;
	}

	/**
	 * @see LoyaltyPaymentGatewayService#pay(String, BigDecimal).
	 */
	@Override
	public boolean pay(final String membershipId, final BigDecimal amount)
			throws LoyaltyTransactionException
	{
		final WSLoyaltyRequest dto = new WSLoyaltyRequest();

		boolean success;

		try
		{
			final WSLoyaltyInvaderZimResponse response =
					sendRequest(dto, WSLoyaltyInvaderZimResponse.class);

			LOG.trace("Response for user {} - remaining: {}, transaction: {}, status: {}",
					membershipId, response.getRemainingAmount(), response.getTransaction(), response.isSuccessful());

			success = response.isSuccessful();
		}
		catch (final Exception ex)
		{
			throw new LoyaltyTransactionException("Error while trying to call the Garfield service", ex);
		}

		return success;
	}
}
