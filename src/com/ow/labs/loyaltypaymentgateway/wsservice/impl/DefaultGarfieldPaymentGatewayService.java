package com.ow.labs.loyaltypaymentgateway.wsservice.impl;

import com.ow.labs.loyaltypaymentgateway.facades.dtos.WSLoyaltyGarfieldResponse;
import com.ow.labs.loyaltypaymentgateway.facades.dtos.WSLoyaltyRequest;
import com.ow.labs.loyaltypaymentgateway.wsservice.LoyaltyPaymentGatewayService;
import com.ow.labs.loyaltypaymentgateway.wsservice.exceptions.LoyaltyTransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.DecimalFormat;


/**
 * @author carlos-ow.
 */
public class DefaultGarfieldPaymentGatewayService extends AbstractLoyaltyPaymentGatewayService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultGarfieldPaymentGatewayService.class);

	private static final String SUCCESS_STATUS = "success";

	protected Logger getLog()
	{
		return LOG;
	}

	/**
	 * @see LoyaltyPaymentGatewayService#pay(String, BigDecimal).
	 */
	@Override
	public boolean pay(final String membershipId, final BigDecimal amount)
			throws LoyaltyTransactionException
	{
		final WSLoyaltyRequest dto = new WSLoyaltyRequest();
		dto.setMembership(membershipId);

		// format the amount for the DTO
		final DecimalFormat formatter = new DecimalFormat();
		formatter.setMaximumFractionDigits(2);
		dto.setAmount(formatter.format(amount));

		boolean success;

		try
		{
			final WSLoyaltyGarfieldResponse response =
					sendRequest(dto, WSLoyaltyGarfieldResponse.class);

			LOG.trace("Response for user {} - status: {}, transaction: {}",
					membershipId, response.getStatus(), response.getTransactionId());

			success = SUCCESS_STATUS.equals(response.getStatus());
		}
		catch (final Exception ex)
		{
			throw new LoyaltyTransactionException("Error while trying to call the Garfield service", ex);
		}

		return success;
	}
}
