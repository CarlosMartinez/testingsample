package com.ow.labs.loyaltypaymentgateway.wsservice.impl;

import com.ow.labs.loyaltypaymentgateway.facades.dtos.WSLoyaltyRequest;
import com.ow.labs.loyaltypaymentgateway.facades.dtos.WSLoyaltyRocoResponse;
import com.ow.labs.loyaltypaymentgateway.wsservice.LoyaltyPaymentGatewayService;
import com.ow.labs.loyaltypaymentgateway.wsservice.exceptions.LoyaltyTransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;


/**
 * @author carlos-ow.
 */
public class DefaultRocoPaymentGatewayService extends AbstractLoyaltyPaymentGatewayService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultRocoPaymentGatewayService.class);

	protected Logger getLog()
	{
		return LOG;
	}

	/**
	 * @see LoyaltyPaymentGatewayService#pay(String, BigDecimal).
	 */
	@Override
	public boolean pay(final String membershipId, final BigDecimal amount)
			throws LoyaltyTransactionException
	{
		final WSLoyaltyRequest dto = new WSLoyaltyRequest();

		boolean success;

		try
		{
			final WSLoyaltyRocoResponse response =
					sendRequest(dto, WSLoyaltyRocoResponse.class);

			LOG.trace("Response for user {} - status: {}, transaction: {}, remaining: {}",
					membershipId, response.isSuccessful(), response.getTransactionId(), response.getRemaining());

			success = response.isSuccessful();
		}
		catch (final Exception ex)
		{
			throw new LoyaltyTransactionException("Error while trying to call the Garfield service", ex);
		}

		return success;
	}
}
