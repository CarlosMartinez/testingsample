package com.ow.labs.loyaltypaymentgateway.wsservice.impl;

import com.ow.labs.loyaltypaymentgateway.facades.dtos.WSLoyaltyRequest;
import com.ow.labs.loyaltypaymentgateway.facades.populators.WSLoyaltyPaymentGatewayPopulator;
import com.ow.labs.loyaltypaymentgateway.wsservice.LoyaltyPaymentGatewayService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;


/**
 * @author carlos-ow.
 */
public abstract class AbstractLoyaltyPaymentGatewayService implements LoyaltyPaymentGatewayService
{
	private WSLoyaltyPaymentGatewayPopulator populator;
	private String endpoint;

	/**
	 * Makes the HTTP call to the given endpoint.
	 *
	 * @param request        Request to be sent.
	 * @param responseType   Class of the response that will be casted to.
	 * @param <ResponseType> Response type for internal casting.
	 * @return Response of the WS call.
	 * @throws Exception
	 */
	protected <ResponseType> ResponseType sendRequest(final WSLoyaltyRequest request,
			final Class<ResponseType> responseType) throws Exception
	{
		getLog().info("Serializing the request to be sent");

		final String body = getPopulator().createRequest(request);

		getLog().trace("Request {}", body);

		// send the HTTP request
		final RestTemplate client = new RestTemplate();
		final HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		final HttpEntity reqEntity = new HttpEntity<>(body, headers);
		return client
				.exchange(getEndpoint(), HttpMethod.POST, reqEntity, responseType)
				.getBody();
	}

	abstract Logger getLog();


	public WSLoyaltyPaymentGatewayPopulator getPopulator()
	{
		return populator;
	}

	@Required
	public void setPopulator(WSLoyaltyPaymentGatewayPopulator populator)
	{
		this.populator = populator;
	}

	public String getEndpoint()
	{
		return endpoint;
	}

	@Required
	public void setEndpoint(String endpoint)
	{
		this.endpoint = endpoint;
	}
}
