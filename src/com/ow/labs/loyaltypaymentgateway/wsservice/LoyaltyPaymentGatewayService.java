package com.ow.labs.loyaltypaymentgateway.wsservice;

import com.ow.labs.loyaltypaymentgateway.wsservice.exceptions.LoyaltyTransactionException;

import java.math.BigDecimal;


/**
 * @author carlos-ow.
 */
public interface LoyaltyPaymentGatewayService
{
	/**
	 * Pays to a specific loyalty gateway serivce.
	 *
	 * @param membershipId User's membership.
	 * @param amount       Amount to be paid.
	 * @return If the transaction could be applied or not.
	 * @throws LoyaltyTransactionException
	 */
	boolean pay(final String membershipId, final BigDecimal amount) throws LoyaltyTransactionException;
}
