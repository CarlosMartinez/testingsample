package com.ow.labs.loyaltypaymentgateway.wsservice.exceptions;

/**
 * @author carlos-ow.
 */
public class LoyaltyTransactionException extends Exception
{
	public LoyaltyTransactionException()
	{
		// empty one
	}

	public LoyaltyTransactionException(final String message)
	{
		super(message);
	}

	public LoyaltyTransactionException(final String message, final Throwable throwable)
	{
		super(message, throwable);
	}
}
