package com.ow.labs.loyaltypaymentgateway.controller;

import com.ow.labs.loyaltypaymentgateway.constants.LoyaltypaymentgatewayConstants.PaymentGateways;
import com.ow.labs.loyaltypaymentgateway.data.GenericResponseDto;
import com.ow.labs.loyaltypaymentgateway.facades.LoyaltyPaymentGatewayFacade;
import com.ow.labs.loyaltypaymentgateway.wsservice.exceptions.LoyaltyTransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * @author carlos-ow.
 */
@RestController
@RequestMapping("/loyalty/payment")
public class LoyaltyController
{
	private static final Logger LOG = LoggerFactory.getLogger(LoyaltyController.class);

	@Resource(name = "loyaltyPaymentGatewayFacade")
	private LoyaltyPaymentGatewayFacade loyaltyPaymentGatewayFacade;

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String testt()
	{
		return "hmmm";
	}

	/**
	 * Executes the payment with a given payment gateway for the current user.
	 *
	 * @param gateway Gateway to be used
	 * @return
	 */
	@RequestMapping(value = "/gateways/{gateway}", method = RequestMethod.POST)
	public GenericResponseDto payWithLoyaltyPoints(@PathVariable final String gateway,
			@RequestParam final BigDecimal amount)
	{
		final GenericResponseDto response = new GenericResponseDto();
		response.setSuccess(Boolean.FALSE);

		try
		{
			final boolean success = loyaltyPaymentGatewayFacade.pay(gateway, amount);
			response.setSuccess(success);
			// TODO: get localized messages for each of the elements on the transaction
			response.setError("Could not execute the transaction");
		}
		catch (final LoyaltyTransactionException ex)
		{
			LOG.error("Error while trying to execute the transaction", ex);
			response.setError("Some useful message for the frontend");
		}
		catch (final Exception ex)
		{
			LOG.error("Generic error caught while executing the transaction", ex);
			response.setError("Some useful message for the frontend");
		}

		return response;
	}
}
